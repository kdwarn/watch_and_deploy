use std::collections::VecDeque;
use std::error::Error;
use std::fs::OpenOptions;
use std::process::{Command, Stdio};
use std::{thread, time};

use dotenvy_macro::dotenv;
use log::{debug, error, info};
use serde::{Deserialize, Serialize};
use simplelog::*;

// private repos will need BEARER tokens
const COMMAND: &str = dotenv!("COMMAND");
// ARGS will probably need to be parsed
const ARGS: &[&str] = &[dotenv!("ARGS")];
const URL: &str = dotenv!("URL");
const OWNER: &str = dotenv!("OWNER");
const REPO: &str = dotenv!("REPO");

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Commit {
    sha: String,
}

fn get_commit() -> Result<Option<Commit>, Box<dyn Error>> {
    // GH: <https://docs.github.com/en/rest/commits/commits?apiVersion=2022-11-28#list-commits>

    let endpoint = &format!("{URL}/{OWNER}/{REPO}/commits?per_page=2");

    let mut res: VecDeque<Commit> = ureq::get(endpoint)
        .set("Accept", "application/vnd.github+json")
        .call()?
        .into_json()?;

    match res.is_empty() {
        true => Ok(None),
        false => Ok(Some(res.pop_front().unwrap())),
    }
}

/// Run the deployment program
fn deploy() {
    // just a dummy external program for now,
    // but will eventually be call to deployment program
    Command::new(COMMAND)
        .args(ARGS)
        .stderr(Stdio::null())
        .spawn()
        .expect("Command failed to start.");
}

fn main() {
    // Build the Config to be used, first trying to set the local offset.
    // If it fails, don't use the offset.
    let config = if let Ok(v) = ConfigBuilder::new()
        .set_time_format_custom(format_description!(
            "[year]-[month]-[day] [hour]:[minute]:[second] \
            [offset_hour sign:mandatory][offset_minute]"
        ))
        .set_time_offset_to_local()
    {
        v.build()
    } else {
        ConfigBuilder::new()
            .set_time_format_custom(format_description!(
                "[year]-[month]-[day] [hour]:[minute]:[second] \
                [offset_hour sign:mandatory][offset_minute]"
            ))
            .build()
    };

    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Warn,
            config.clone(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Info,
            config,
            OpenOptions::new()
                .append(true)
                .create(true)
                .open("watch_and_deploy.log")
                .unwrap(),
        ),
    ])
    .unwrap();

    let one_minute = time::Duration::from_secs(60);
    let mut commit: Option<String> = None;
    info!("Started watching {REPO}.");
    loop {
        match get_commit() {
            Ok(Some(v)) => {
                if commit.is_none() {
                    // Assume that this is not the initial deployment
                    // but just the initialization of the program.
                    // So no deployment, just updating var.
                    commit = Some(v.sha);
                    info!("Most recent commit SHA recorded.");
                } else if commit != Some(v.sha.clone()) {
                    info!("Deploying...");
                    deploy();
                    commit = Some(v.sha);
                } else {
                    info!("No new commit detected, doing nothing.");
                }
            }
            Ok(None) => info!("No commits/SHAs found."),
            Err(e) => error!("Error: {e}"),
        }
        thread::sleep(one_minute);
    }
}
